const express = require('express');
const app = express();
const path = require('path');

// Configurando o EJS como engine de visualização
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

// Rota principal
app.get('/', (req, res) => {
    res.render('template', { pageTitle: 'Minha Página Node.js', content: '<h2>Conteúdo Principal</h2><p>Conteúdo da página principal.</p>' });
});

// Rota para Seção 1
app.get('/secao1', (req, res) => {
    res.render('template', { pageTitle: 'Seção 1', content: '<h2>Conteúdo da Seção 1</h2><p>Conteúdo da seção 1.</p>' });
});

// Rota para Seção 2
app.get('/secao2', (req, res) => {
    res.render('template', { pageTitle: 'Seção 2', content: '<h2>Conteúdo da Seção 2</h2><p>Conteúdo da seção 2.</p>' });
});

const port = 3000;
app.listen(port, () => {
    console.log(`Servidor rodando em http://localhost:${port}`);
});
