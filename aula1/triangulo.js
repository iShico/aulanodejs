function triangulo1(a,b,c){
 if(a!=0 && b!=0 && c!=0){
   if(Math.abs(a-b)<c && c<(a+b) ){
    if(Math.abs(a-c)<b && b<(a+c) ){
        if(Math.abs(c-b)<a && a<(c+b) ){
            console.log("É triangulo");
        }else console.log("Não é triangulo");
    }else console.log("Não é triangulo");
   }else console.log("Não é triangulo");
 }else console.log("Não pode haver valor nulo");

}

triangulo1(1, 2, 3);
triangulo1(1, 2, 2);
triangulo1(1, 0, 3);

function teste(a,b,c){
    if(Math.abs(a-b)<c && c<(a+b))return true;
    else return false;
}

function triangulo2(a,b,c){
    if( a != 0 && b != 0 && c != 0){
        if(teste(a,b,c) && teste(a,c,b) && teste(c,b,a)) console.log("é triangulo");
        else console.log("Não é triangulo");
    }else console.log("Não pode haver valor nulo");

}
triangulo2(1, 2, 3);
triangulo2(1, 2, 2);
triangulo2(1, 0, 3); 